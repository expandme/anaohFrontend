# frontendAnaoh

*This is the frontend for anaoh built with Angularjs*

## Setup
1. Install [Node.js](http://nodejs.org/)
 - This will also install npm.
2. Run `npm install -g bower gulp yo generator-ng-poly@0.10.11`
 - This enables Bower, Gulp, and Yeoman generators to be used from command line.
3. Run `npm install` to install this project's dependencies
4. Run `bower install` to install client-side dependencies

## Gulp tasks
- Run `gulp build` to compile assets
- Run `gulp dev` to run the build task and setup the development environment
- Run `gulp unitTest` to run unit tests via Karma and to create code coverage reports
- Run `gulp webdriverUpdate` to download Selenium server standalone and Chrome driver for e2e testing
- Run `gulp e2eTest` to run e2e tests via Protractor
 - **A localhost must be running** - `gulp dev`
- Run `gulp build --stage=prod` to minifiy/concate/optimize css,js,html,images
