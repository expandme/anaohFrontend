/* global describe, beforeEach, it, browser, expect */
'use strict';

var BeforeFundingPagePo = require('./before-funding.po');

describe('Before funding page', function () {
  var beforeFundingPage;

  beforeEach(function () {
    beforeFundingPage = new BeforeFundingPagePo();
    browser.get('/#/before-funding');
  });

  it('should say BeforeFundingCtrl', function () {
    expect(beforeFundingPage.heading.getText()).toEqual('beforeFunding');
    expect(beforeFundingPage.text.getText()).toEqual('BeforeFundingCtrl');
  });
});
