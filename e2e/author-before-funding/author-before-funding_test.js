/* global describe, beforeEach, it, browser, expect */
'use strict';

var AuthorBeforeFundingPagePo = require('./author-before-funding.po');

describe('Author before funding page', function () {
  var authorBeforeFundingPage;

  beforeEach(function () {
    authorBeforeFundingPage = new AuthorBeforeFundingPagePo();
    browser.get('/#/author-before-funding');
  });

  it('should say AuthorBeforeFundingCtrl', function () {
    expect(authorBeforeFundingPage.heading.getText()).toEqual('authorBeforeFunding');
    expect(authorBeforeFundingPage.text.getText()).toEqual('AuthorBeforeFundingCtrl');
  });
});
