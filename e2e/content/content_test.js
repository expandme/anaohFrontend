/* global describe, beforeEach, it, browser, expect */
'use strict';

var ContentPagePo = require('./content.po');

describe('Content page', function () {
  var contentPage;

  beforeEach(function () {
    contentPage = new ContentPagePo();
    browser.get('/#/content');
  });

  it('should say ContentCtrl', function () {
    expect(contentPage.heading.getText()).toEqual('content');
    expect(contentPage.text.getText()).toEqual('ContentCtrl');
  });
});
