/* global describe, beforeEach, it, browser, expect */
'use strict';

var UserBeforeFundingPagePo = require('./user-before-funding.po');

describe('User before funding page', function () {
  var userBeforeFundingPage;

  beforeEach(function () {
    userBeforeFundingPage = new UserBeforeFundingPagePo();
    browser.get('/#/user-before-funding');
  });

  it('should say UserBeforeFundingCtrl', function () {
    expect(userBeforeFundingPage.heading.getText()).toEqual('userBeforeFunding');
    expect(userBeforeFundingPage.text.getText()).toEqual('UserBeforeFundingCtrl');
  });
});
