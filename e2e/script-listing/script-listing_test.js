/* global describe, beforeEach, it, browser, expect */
'use strict';

var ScriptListingPagePo = require('./script-listing.po');

describe('Script listing page', function () {
  var scriptListingPage;

  beforeEach(function () {
    scriptListingPage = new ScriptListingPagePo();
    browser.get('/#/script-listing');
  });

  it('should say ScriptListingCtrl', function () {
    expect(scriptListingPage.heading.getText()).toEqual('scriptListing');
    expect(scriptListingPage.text.getText()).toEqual('ScriptListingCtrl');
  });
});
