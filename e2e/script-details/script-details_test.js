/* global describe, beforeEach, it, browser, expect */
'use strict';

var ScriptDetailsPagePo = require('./script-details.po');

describe('Script details page', function () {
  var scriptDetailsPage;

  beforeEach(function () {
    scriptDetailsPage = new ScriptDetailsPagePo();
    browser.get('/#/script-details');
  });

  it('should say ScriptDetailsCtrl', function () {
    expect(scriptDetailsPage.heading.getText()).toEqual('scriptDetails');
    expect(scriptDetailsPage.text.getText()).toEqual('ScriptDetailsCtrl');
  });
});
