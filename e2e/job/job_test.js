/* global describe, beforeEach, it, browser, expect */
'use strict';

var JobPagePo = require('./job.po');

describe('Job page', function () {
  var jobPage;

  beforeEach(function () {
    jobPage = new JobPagePo();
    browser.get('/#/job');
  });

  it('should say JobCtrl', function () {
    expect(jobPage.heading.getText()).toEqual('job');
    expect(jobPage.text.getText()).toEqual('JobCtrl');
  });
});
