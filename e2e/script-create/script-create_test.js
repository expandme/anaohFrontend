/* global describe, beforeEach, it, browser, expect */
'use strict';

var ScriptCreatePagePo = require('./script-create.po');

describe('Script create page', function () {
  var scriptCreatePage;

  beforeEach(function () {
    scriptCreatePage = new ScriptCreatePagePo();
    browser.get('/#/script-create');
  });

  it('should say ScriptCreateCtrl', function () {
    expect(scriptCreatePage.heading.getText()).toEqual('scriptCreate');
    expect(scriptCreatePage.text.getText()).toEqual('ScriptCreateCtrl');
  });
});
