/* global describe, beforeEach, it, browser, expect */
'use strict';

var WallPagePo = require('./wall.po');

describe('Wall page', function () {
  var wallPage;

  beforeEach(function () {
    wallPage = new WallPagePo();
    browser.get('/#/wall');
  });

  it('should say WallCtrl', function () {
    expect(wallPage.heading.getText()).toEqual('wall');
    expect(wallPage.text.getText()).toEqual('WallCtrl');
  });
});
