(function () {
  'use strict';

  /* @ngdoc object
   * @name content
   * @description
   *
   */
  angular
    .module('content', [
      'ui.router'
    ]);
}());
