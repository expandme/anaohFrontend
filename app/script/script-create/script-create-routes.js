(function () {
  'use strict';

  angular
    .module('script.scriptCreate')
    .config(config);

  function config($stateProvider) {
    $stateProvider
      .state('scriptCreate', {
        url: '/script/create',
        templateUrl: 'script/script-create/script-create.tpl.html',
        controller: 'ScriptCreateCtrl',
        controllerAs: 'scriptCreate'
      });
  }
}());
