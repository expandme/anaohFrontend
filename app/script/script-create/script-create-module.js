(function () {
  'use strict';

  /* @ngdoc object
   * @name script.scriptCreate
   * @description
   *
   */
  angular
    .module('script.scriptCreate', [
      'ui.router'
    ]);
}());
