/* global describe, beforeEach, it, expect, inject, module */
'use strict';

describe('ScriptCreateCtrl', function () {
  var ctrl;

  beforeEach(module('script.scriptCreate'));

  beforeEach(inject(function ($rootScope, $controller) {
    ctrl = $controller('ScriptCreateCtrl');
  }));

  it('should have ctrlName as ScriptCreateCtrl', function () {
    expect(ctrl.ctrlName).toEqual('ScriptCreateCtrl');
  });
});
