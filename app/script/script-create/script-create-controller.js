(function () {
  'use strict';

  /**
   * @ngdoc object
   * @name script.scriptCreate.controller:ScriptCreateCtrl
   *
   * @description
   *
   */
  angular
    .module('script.scriptCreate')
    .controller('ScriptCreateCtrl', ScriptCreateCtrl);

  function ScriptCreateCtrl(AuthToken, $scope, Asset, UserRole, SweetAlert, Product) {

    $scope.isAuthenticated = AuthToken.isAuthenticated;

    return getGenreGeo().then(function() {
      associatedUserRoles();
    }).then(function() {
      renderUi();
    });

    function associatedUserRoles() {
      return UserRole.associatedUserRoles().then(function(data) {
        $scope.associatedRoles = data.userRoleDetails;
      });
    }

    function getGenreGeo() {
      return Asset.listGenreGeo().then(function(data) {
        console.log(data);
        $scope.genres = data.Genre;
        $scope.locations = data.Geo;
        $scope.languages = data.Language;
      });
    }

    function renderUi() {

      $scope.createScript = function(script) {
        console.log(script);
        Product.createScript(script)
        .then(function (res) {
          console.log(res);
          SweetAlert.swal("Congratulation!", "Your script created successfully", "success");
        })
        .error(function (err) {
          console.log(err)
          SweetAlert.swal("Error!", err, "warning");
        });

      }

    }
  }
}());
