(function () {
  'use strict';

  angular
    .module('scriptListing.beforeFunding')
    .config(config);

  function config($stateProvider) {
    $stateProvider
      .state('beforeFunding', {
        url: '/script/before-funding',
        templateUrl: 'script/script-listing/before-funding/before-funding.tpl.html',
        controller: 'BeforeFundingCtrl',
        controllerAs: 'beforeFunding'
      });
  }
}());
