/* global describe, beforeEach, it, expect, inject, module */
'use strict';

describe('BeforeFundingCtrl', function () {
  var ctrl;

  beforeEach(module('scriptListing.beforeFunding'));

  beforeEach(inject(function ($rootScope, $controller) {
    ctrl = $controller('BeforeFundingCtrl');
  }));

  it('should have ctrlName as BeforeFundingCtrl', function () {
    expect(ctrl.ctrlName).toEqual('BeforeFundingCtrl');
  });
});
