(function () {
  'use strict';

  /* @ngdoc object
   * @name scriptListing.beforeFunding
   * @description
   *
   */
  angular
    .module('scriptListing.beforeFunding', [
      'ui.router'
    ]);
}());
