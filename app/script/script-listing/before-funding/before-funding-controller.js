(function () {
  'use strict';

  /**
   * @ngdoc object
   * @name scriptListing.beforeFunding.controller:BeforeFundingCtrl
   *
   * @description
   *
   */
  angular
    .module('scriptListing.beforeFunding')
    .controller('BeforeFundingCtrl', BeforeFundingCtrl);

  function BeforeFundingCtrl() {
    var vm = this;
    vm.ctrlName = 'BeforeFundingCtrl';
  }
}());
