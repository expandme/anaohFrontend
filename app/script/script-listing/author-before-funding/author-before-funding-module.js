(function () {
  'use strict';

  /* @ngdoc object
   * @name scriptListing.authorBeforeFunding
   * @description
   *
   */
  angular
    .module('scriptListing.authorBeforeFunding', [
      'ui.router'
    ]);
}());
