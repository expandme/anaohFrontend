(function () {
  'use strict';

  angular
    .module('scriptListing.authorBeforeFunding')
    .config(config);

  function config($stateProvider) {
    $stateProvider
      .state('authorBeforeFunding', {
        url: '/script/author-before-funding',
        templateUrl: 'script/script-listing/author-before-funding/author-before-funding.tpl.html',
        controller: 'AuthorBeforeFundingCtrl',
        controllerAs: 'authorBeforeFunding'
      });
  }
}());
