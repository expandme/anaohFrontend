/* global describe, beforeEach, it, expect, inject, module */
'use strict';

describe('AuthorBeforeFundingCtrl', function () {
  var ctrl;

  beforeEach(module('scriptListing.authorBeforeFunding'));

  beforeEach(inject(function ($rootScope, $controller) {
    ctrl = $controller('AuthorBeforeFundingCtrl');
  }));

  it('should have ctrlName as AuthorBeforeFundingCtrl', function () {
    expect(ctrl.ctrlName).toEqual('AuthorBeforeFundingCtrl');
  });
});
