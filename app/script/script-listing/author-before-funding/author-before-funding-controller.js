(function () {
  'use strict';

  /**
   * @ngdoc object
   * @name scriptListing.authorBeforeFunding.controller:AuthorBeforeFundingCtrl
   *
   * @description
   *
   */
  angular
    .module('scriptListing.authorBeforeFunding')
    .controller('AuthorBeforeFundingCtrl', AuthorBeforeFundingCtrl);

  function AuthorBeforeFundingCtrl() {
    var vm = this;
    vm.ctrlName = 'AuthorBeforeFundingCtrl';
  }
}());
