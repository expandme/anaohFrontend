(function () {
  'use strict';

  /* @ngdoc object
   * @name scriptListing.userBeforeFunding
   * @description
   *
   */
  angular
    .module('scriptListing.userBeforeFunding', [
      'ui.router'
    ]);
}());
