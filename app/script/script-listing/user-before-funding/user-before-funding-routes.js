(function () {
  'use strict';

  angular
    .module('scriptListing.userBeforeFunding')
    .config(config);

  function config($stateProvider) {
    $stateProvider
      .state('userBeforeFunding', {
        url: '/script/user-before-funding',
        templateUrl: 'script/script-listing/user-before-funding/user-before-funding.tpl.html',
        controller: 'UserBeforeFundingCtrl',
        controllerAs: 'userBeforeFunding'
      });
  }
}());
