/* global describe, beforeEach, it, expect, inject, module */
'use strict';

describe('UserBeforeFundingCtrl', function () {
  var ctrl;

  beforeEach(module('scriptListing.userBeforeFunding'));

  beforeEach(inject(function ($rootScope, $controller) {
    ctrl = $controller('UserBeforeFundingCtrl');
  }));

  it('should have ctrlName as UserBeforeFundingCtrl', function () {
    expect(ctrl.ctrlName).toEqual('UserBeforeFundingCtrl');
  });
});
