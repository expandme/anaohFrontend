(function () {
  'use strict';

  /**
   * @ngdoc object
   * @name scriptListing.userBeforeFunding.controller:UserBeforeFundingCtrl
   *
   * @description
   *
   */
  angular
    .module('scriptListing.userBeforeFunding')
    .controller('UserBeforeFundingCtrl', UserBeforeFundingCtrl);

  function UserBeforeFundingCtrl() {
    var vm = this;
    vm.ctrlName = 'UserBeforeFundingCtrl';
  }
}());
