(function () {
  'use strict';

  /* @ngdoc object
   * @name script.scriptListing
   * @description
   *
   */
  angular
    .module('script.scriptListing', [
      'ui.router',
      'scriptListing.beforeFunding',
      'scriptListing.authorBeforeFunding',
      'scriptListing.userBeforeFunding'
    ]);
}());
