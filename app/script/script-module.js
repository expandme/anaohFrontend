(function () {
  'use strict';

  /* @ngdoc object
   * @name script
   * @description
   *
   */
  angular
    .module('script', [
      'ui.router',
      'script.scriptCreate',
      'script.scriptListing',
      'script.scriptDetails'
    ]);
}());
