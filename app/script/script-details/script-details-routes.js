(function () {
  'use strict';

  angular
    .module('script.scriptDetails')
    .config(config);

  function config($stateProvider) {
    $stateProvider
      .state('scriptDetails', {
        url: '/script/:id',
        templateUrl: 'script/script-details/script-details.tpl.html',
        controller: 'ScriptDetailsCtrl',
        controllerAs: 'scriptDetails'
      });
  }
}());
