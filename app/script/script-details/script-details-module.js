(function () {
  'use strict';

  /* @ngdoc object
   * @name script.scriptDetails
   * @description
   *
   */
  angular
    .module('script.scriptDetails', [
      'ui.router'
    ]);
}());
