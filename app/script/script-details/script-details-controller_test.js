/* global describe, beforeEach, it, expect, inject, module */
'use strict';

describe('ScriptDetailsCtrl', function () {
  var ctrl;

  beforeEach(module('script.scriptDetails'));

  beforeEach(inject(function ($rootScope, $controller) {
    ctrl = $controller('ScriptDetailsCtrl');
  }));

  it('should have ctrlName as ScriptDetailsCtrl', function () {
    expect(ctrl.ctrlName).toEqual('ScriptDetailsCtrl');
  });
});
