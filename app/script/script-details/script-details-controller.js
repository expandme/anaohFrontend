(function () {
  'use strict';

  /**
   * @ngdoc object
   * @name script.scriptDetails.controller:ScriptDetailsCtrl
   *
   * @description
   *
   */
   angular
   .module('script.scriptDetails')
   .controller('ScriptDetailsCtrl', ScriptDetailsCtrl);

   function ScriptDetailsCtrl(AuthToken, Product, $scope, $stateParams, Auth, SweetAlert, $timeout) {

    $scope.isAuthenticated = AuthToken.isAuthenticated;
    $scope.loading = false;
    $scope.appointmentProgress = false;
    $scope.investmentProgress = false;
    $scope.approveRequest = false;
    $scope.recommendModalShown = false;

    $scope.recommend = {};
    $scope.isReadonly = false; // default test value
    $scope.changeOnHover = true; // default test value
    $scope.maxValue = 10; // default test value
    $scope.recommend.ratingValue = 0; // default test value

    $scope.datepickerConfig = {
      allowFuture: true,
      dateFormat: 'YYYY-MM-DD'
    };

    var scriptId = $stateParams.id;

    if ($scope.isAuthenticated()) {
      Auth.isAuthor(scriptId).then(function(data) {
        if(data.data.isAuthor) {
          $scope.isAuthor = true;
          Product.connectionRequests(scriptId).then(function(data) {
            console.log(data);
            $scope.connectionRequests = data.data;
          });
        }
      });
    };

    $scope.connect = function() {
      if(!$scope.isAuthenticated()) SweetAlert.swal("Error!", 'please login to connect', 'error');
      else $scope.loading = true;
      Product.scriptConnection(scriptId).then(function (data) {
        SweetAlert.swal("Status!", data.data.message, data.data.status);
        $scope.loading = false;
      });
    }

    $scope.approve = function(request, index) {
      if(!$scope.isAuthenticated()) SweetAlert.swal("Error!", 'please login to connect', 'error');
      else $scope.approveRequest = true;
      Product.approveConnection(request).then(function (data) {
        SweetAlert.swal("Status!", data.data.message, data.data.status);
        $scope.approveRequest = false;
        $scope.connectionRequests.list.splice(index, 1);
      });
    }

    $scope.gauge = {
      value: 75,
    };

    $scope.recommedModal = function() {
      if(!$scope.isAuthenticated()) SweetAlert.swal("Error!", 'please login to give recommendation', 'error');
      else $scope.recommendModalShown = !$scope.recommendModalShown;
    };

    $scope.giveRecommend = function(recommendData) {
      recommendData.scriptId = scriptId;
      console.log(recommendData);
      Product.recommendation(recommendData).then(function(data) {
        $timeout(function() {
          $('.ng-modal-close').trigger('click');
        });
        SweetAlert.swal("Status!", data.data.message, data.data.status);
      });
    }

    $scope.makeAppointment = function(data) {
      if(!$scope.isAuthenticated()) SweetAlert.swal("Error!", 'please login to make an appointment', 'error');
      else $scope.appointmentProgress = true;
      data.scriptId = scriptId;
      if(!data.remarks) data.remarks = "no remarks"
      Product.makeAppointment(data).then(function (data) {
        SweetAlert.swal("Status!", data.data.message, data.data.status);
        $scope.appointmentProgress = false;
      });
    }

    $scope.makeInvestment = function(data) {
      if(!$scope.isAuthenticated()) SweetAlert.swal("Error!", 'please login to make an investment', 'error');
      else $scope.investmentProgress = true;
      data.scriptId = scriptId;
      if(!data.remarks) data.remarks = "no remarks"
      Product.makeInvestment(data).then(function (data) {
        SweetAlert.swal("Status!", data.data.message, data.data.status);
        $scope.investmentProgress = false;
      });
    }

    $(".tabbable").on("click", ".nav-tabs > li", function() {
      if ($(this).hasClass("disable")) return false;
      var e = $(this).index();
      $(this).siblings().removeClass("active").end().addClass("active");
      $(this).parents(".tabbable").find(".tab-content .tab-pane").removeClass("active").eq(e).addClass("active");
      return false
    });

    $(".accordion").on("click", ".accordion-label", function() {
      if ($(this).hasClass("active")) {
        $(this).removeClass("active").siblings(".accordion-content").slideUp(400, function() {
          $(this).removeClass("active").removeAttr("style")
        })
      } else {
        $(this).parents(".accordion").find(".accordion-label").removeClass("active");
        $(this).addClass("active").siblings(".accordion-content").slideDown(400, function() {
          $(this).addClass("active").removeAttr("style")
        });
        $(this).parent().siblings().find(".accordion-content").slideUp(400, function() {
          $(this).removeClass("active").removeAttr("style")
        })
      }
      return false
    });

    return getScript().then(function() {
      console.log('geting script details');
    });

    function getScript() {
      return Product.scriptById(scriptId).then(function(data) {
        console.log(data);
        $scope.details = data.data;
        $scope.details.productDetails.url = window.location.href;
      });
    }
  }
}());
