(function () {
  'use strict';

  /**
   * @ngdoc object
   * @name wall.controller:WallCtrl
   *
   * @description
   *
   */
  angular
    .module('wall')
    .controller('WallCtrl', WallCtrl);

  function WallCtrl(AuthToken, Product, $scope, $stateParams, Auth, SweetAlert, $timeout) {

    $scope.wallPost = true;
    $scope.recommendation = false;
    $scope.likes = false;
    var scriptId = $stateParams.id;

    $scope.isReadonly = true; // default test value
    $scope.changeOnHover = true; // default test value
    $scope.maxValue = 10; // default test value

    $scope.loadWall = function() {
      $scope.wallPost = true;
      $scope.recommendation = false;
      $scope.likes = false;
    }

    $scope.loadLikes = function() {
      $scope.wallPost = false;
      $scope.recommendation = false;
      $scope.likes = true;
    }

    $scope.loadRecommendation = function() {
      $scope.wallPost = false;
      $scope.recommendation = true;
      $scope.likes = false;
    }



    $.fn.showLoader = function(){
      $(this).removeClass('hide');
    }
    $.fn.hideLoader = function(){
      $(this).addClass('hide');
    }
    $('.timeline-panel').delegate('#shareType','click', function(e) {
      e.preventDefault();
      var positionArray = {};
      positionArray['status'] = 0;
      positionArray['photos'] = 80;
      positionArray['videos'] = 160;
      positionArray['location'] = 231;
      $('.video').hideLoader('hide');
      $('.image').hideLoader('hide');
       $('.place').hideLoader('hide');
      $('.shareType').val($(this).attr('class'));
      $('.arrow').css("left", positionArray[$(this).attr('class')]);
      if($(this).attr('class') == 'videos') {
        $('.video').showLoader('hide');
        $('.image').hideLoader('hide');
        $('.place').hideLoader('hide');
      }
      if($(this).attr('class') == 'photos') {
        $('.video').hideLoader('hide');
        $('.image').showLoader('hide');
        $('.place').hideLoader('hide');
      }
      return false;
    });

    return getScript().then(function() {
      console.log('geting wall details');
    });

    function getScript() {
      return Product.scriptById(scriptId).then(function(data) {
        console.log(data);
        $scope.details = data.data;
      });
    }
  }
}());
