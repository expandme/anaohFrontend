(function () {
  'use strict';

  /* @ngdoc object
   * @name wall
   * @description
   *
   */
  angular
    .module('wall', [
      'ui.router'
    ]);
}());
