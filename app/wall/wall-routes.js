(function () {
  'use strict';

  angular
    .module('wall')
    .config(config);

  function config($stateProvider) {
    $stateProvider
      .state('wall', {
        url: '/script-wall/:id',
        templateUrl: 'wall/wall.tpl.html',
        controller: 'WallCtrl',
        controllerAs: 'wall'
      });
  }
}());
