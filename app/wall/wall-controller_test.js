/* global describe, beforeEach, it, expect, inject, module */
'use strict';

describe('WallCtrl', function () {
  var ctrl;

  beforeEach(module('wall'));

  beforeEach(inject(function ($rootScope, $controller) {
    ctrl = $controller('WallCtrl');
  }));

  it('should have ctrlName as WallCtrl', function () {
    expect(ctrl.ctrlName).toEqual('WallCtrl');
  });
});
