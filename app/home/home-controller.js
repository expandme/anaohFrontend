(function () {
  'use strict';

  /**
   * @ngdoc object
   * @name home.controller:HomeCtrl
   *
   * @description
   *
   */
  angular
    .module('home')
    .controller('HomeCtrl', HomeCtrl);

  function HomeCtrl(Product, Job, jobsResolved, $scope) {

    var count = 0;
    var jobCount = 0;
    $scope.lazyLoadScripts = false;
    $scope.prevJobList = true;
    $scope.nextJobList = false;
    $scope.gauge = {
      value: 65,
    };

    return getProducts().then(function() {
      loadUiOperations();
    });

    function getProducts() {
      return Product.getProducts(4, count).then(function(data) {
        $scope.products = data.products;
        $scope.inProgressProducts = data.productInProgress;
        var max_fund_index = Array.maxProp(data.products, 'funding');
        $scope.max_funded_product = data.products[max_fund_index];
      });
    }

    function loadUiOperations() {
      $scope.jobs = jobsResolved;
      $scope.loadMore = function() {
        count = count + 4;
        $scope.lazyLoadScripts = true;
        Product.getProducts(4,count).then(function(data) {
          if (data.products.length > 0) {
            $scope.products = $scope.products.concat(data.products);
            $scope.lazyLoadScripts = false;
          };
        });
      }

      $scope.nextJobs = function() {
        jobCount = jobCount + 4;
        Job.listJobs(4, jobCount).then(function (response) {
          if (response.data.length > 0) {
            $scope.jobs = response.data;
          };
          if (response.data.length < 4) {
            $scope.nextJobList = true;
          };
          console.log(response.data);
          if(jobCount > 0)
            $scope.prevJobList = false;
          else
            $scope.prevJobList = true;
        });
      }
      $scope.prevJobs = function() {
        jobCount = jobCount - 4;
        Job.listJobs(4, jobCount).then(function (response) {
          if (response.data.length > 0) {
            $scope.jobs = response.data;
            $scope.nextJobList = false;
          };
          console.log(response.data);
          if(jobCount == 0)
            $scope.prevJobList = true;
          else
            $scope.prevJobList = false;
        });
      }
    }

  }


  /**
   * @maxIndex integer
   * get index of product which get max funding
   *
   * @description
   *
   */
  Array.maxProp = function (array, prop) {

    var values = array.map(function (el) {
      return el[prop];
    });

    var arr = _.values(values);

    var max = arr[0];
    var maxIndex = 0;
    for (var i = 1; i < arr.length; i++) {
      if (arr[i] > max) {
        maxIndex = i;
        max = arr[i];
      }
    }

    return maxIndex;
  };

}());
