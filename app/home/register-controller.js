(function () {
  'use strict';

  /**
   * @ngdoc object
   * @name home.controller:RegisterCtrl
   *
   * @description
   *
   */
  angular
    .module('home')
    .controller('RegisterCtrl', RegisterCtrl);

  function RegisterCtrl(Asset, $scope, $state, SweetAlert, Auth) {

    return getRoleGeo().then(function() {
      renderUi();
    });

  function getRoleGeo() {
    return Asset.listRoleGeo().then(function(data) {
      $scope.roles = data.Role;
      $scope.locations = data.Geo;
    });
  }

  function renderUi() {
    $scope.currentTab = 'account.tpl.html';
    $scope.showRoles = false;

    $scope.onClickTab = function (tab) {
      $scope.currentTab = tab.template;
    }

    $scope.isActiveTab = function(tabUrl) {
      return tabUrl == $scope.currentTab;
    }

    $scope.user = {};

    $scope.registerUser = function(user) {

      user.roles = $scope.user.roles;
      Auth.register(user)
        .success(function (res) {
          $scope.user = res.user;
          $scope.showRoles = true;
          SweetAlert.swal("Congratulation!", "Account Created successfully! Check your email for Activation and add role details", "success");
        })
        .error(function (err) {
          console.log(err)
          SweetAlert.swal("Error!", err.error.message, "warning");
        });
    }

    $scope.submitGeneral = function(general) {

    }

    $scope.submitActor = function(actor) {

    }

    $scope.submitProducer = function(producer) {

    }

    $scope.submitDirector = function(director) {

    }

  }


  }
}());
