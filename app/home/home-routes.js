(function () {
  'use strict';

  angular
    .module('home')
    .config(config);

  function config($stateProvider) {
    $stateProvider
      .state('home', {
        url: '/home',
        templateUrl: 'home/home.tpl.html',
        controller: 'HomeCtrl',
        resolve: {
          jobsResolved: function (Job) {
            return Job.listJobs(4, 0).then(function (response) {
              return response.data;
            });
          }
        }
      })
      .state('register', {
        url: '/register',
        templateUrl: 'home/register.tpl.html',
        controller: 'RegisterCtrl',
      });
  }
}());
