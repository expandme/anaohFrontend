(function () {
  'use strict';

  /* @ngdoc object
   * @name frontendAnaoh
   * @description
   *
   */
  angular
    .module('frontendAnaoh', [
      'ui.router',
      'angular-loading-bar',
      'ngStorage',
      'ngMorph',
      'home',
      'content',
      'common',
      'user',
      'script',
      'job',
      'wall'
    ]);
}());
