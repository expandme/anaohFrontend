(function () {
  'use strict';

  /**
   * @ngdoc service
   * @name job.factory:Job
   *
   * @description
   *
   */
  angular
    .module('job')
    .factory('Job', Job);

  function Job(servicesConfig, $http, $location, $q) {

    var API_URL = servicesConfig.API_URL;

    var service = {
      createJob: createJob,
      listJobs: listJobs,
      viewJob: viewJob,
      applyJob: applyJob
    };

    return service;

    function createJob(job) {
      return $http.post(API_URL + 'jobs', job)
        .then(status);

      function status(data, status, headers, config) {
        return data;
      }
    }

    function listJobs(limit,offset) {
      return $http.get(API_URL + 'jobs?limit='+limit+'&offset='+offset)
        .then(status);

      function status(data, status, headers, config) {
        return data;
      }
    }

    function viewJob(id) {
      return $http.get(API_URL + 'jobs/' + id)
        .then(status);

      function status(data, status, headers, config) {
        return data;
      }
    }

    function applyJob(data) {
      return $http.post(API_URL + 'jobs/apply', data)
        .then(status);

      function status(data, status, headers, config) {
        return data;
      }
    }
  }
}());
