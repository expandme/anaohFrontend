(function () {
  'use strict';

  /**
   * @ngdoc object
   * @name job.controller:JobCtrl
   *
   * @description
   *
   */
  angular
    .module('job')
    .controller('JobCtrl', JobCtrl);

  function JobCtrl(AuthToken, $scope, UserRole, SweetAlert, Job, Asset) {
    $scope.isAuthenticated = AuthToken.isAuthenticated;
    // console.log(jobsResolved);
    return associatedUserProducts().then(function() {
      renderUi();
    });

    function associatedUserProducts() {
      return UserRole.associatedUserProducts().then(function(data) {
        $scope.scripts = data;
      });
    }

    function renderUi() {

      Asset.listJobTypes().then(function(data) {
        console.log(data);
        $scope.jobTypes = data.JobType;
      });

      $scope.createJob = function(job) {
        console.log(job);
        Job.createJob(job)
        .then(function (res) {
          console.log(res);
          SweetAlert.swal("Congratulation!", "Your job created successfully", "success");
        })
        .error(function (err) {
          console.log(err)
          SweetAlert.swal("Error!", err, "warning");
        });

      }
    }
  }
}());
