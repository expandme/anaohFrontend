(function () {
  'use strict';

  angular
    .module('job')
    .config(config);

  function config($stateProvider) {
    $stateProvider
      .state('jobsList', {
        url: '/jobs',
        templateUrl: 'job/jobs.tpl.html',
        controller: 'JobListCtrl',
        resolve: {
          jobsResolved: function (Job) {
            return Job.listJobs().then(function (response) {
              return response.data;
            });
          }
        }
      })

      .state('createJob', {
        url: '/jobs/create',
        templateUrl: 'job/create.tpl.html',
        controller: 'JobCtrl'
      })

      .state('jobDetails', {
        url: '/jobs/:scriptId/:id',
        templateUrl: 'job/job-details.tpl.html',
        controller: 'JobDetailsCtrl',
        resolve: {
          jobDetailsResolved: function (Job, $stateParams) {
            return Job.viewJob($stateParams.id).then(function (response) {
              return response.data;
            });
          }
        }
      });
    }
}());
