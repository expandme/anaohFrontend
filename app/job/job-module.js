(function () {
  'use strict';

  /* @ngdoc object
   * @name job
   * @description
   *
   */
  angular
    .module('job', [
      'ui.router',
      'textAngular'
    ]);
}());
