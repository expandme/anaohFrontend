/* global describe, beforeEach, it, expect, inject, module */
'use strict';

describe('JobCtrl', function () {
  var ctrl;

  beforeEach(module('job'));

  beforeEach(inject(function ($rootScope, $controller) {
    ctrl = $controller('JobCtrl');
  }));

  it('should have ctrlName as JobCtrl', function () {
    expect(ctrl.ctrlName).toEqual('JobCtrl');
  });
});
