(function () {
  'use strict';

  /**
   * @ngdoc object
   * @name job.controller:JobCtrl
   *
   * @description
   *
   */
  angular
    .module('job')
    .controller('JobListCtrl', JobListCtrl);

  function JobListCtrl(AuthToken, $scope, UserRole, SweetAlert, Job, Asset, jobsResolved) {
    $scope.isAuthenticated = AuthToken.isAuthenticated;
    $scope.jobs = jobsResolved;
    console.log(jobsResolved);
  }
}());
