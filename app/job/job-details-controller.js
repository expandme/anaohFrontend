(function () {
  'use strict';

  /**
   * @ngdoc object
   * @name job.controller:JobCtrl
   *
   * @description
   *
   */
  angular
    .module('job')
    .controller('JobDetailsCtrl', JobDetailsCtrl);

  function JobDetailsCtrl(AuthToken, $scope, UserRole, $stateParams, SweetAlert, Job, Asset, jobDetailsResolved) {
    $scope.isAuthenticated = AuthToken.isAuthenticated;
    console.log(jobDetailsResolved);
    $scope.job = jobDetailsResolved;

    $scope.applyJob = function() {
      if(!$scope.isAuthenticated()) SweetAlert.swal("Error!", 'please login first', 'error');
      else {
        var data = {};
        data.resume = $scope.resume;
        data.jobId = $stateParams.id;
        data.productId = $stateParams.scriptId;

        Job.applyJob(data)
        .then(function (res) {
          console.log(res);
          SweetAlert.swal("Congratulation!", "Your application submitted successfully", "success");
        })
      }
    }
  }
}());
