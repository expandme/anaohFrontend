/* global describe, beforeEach, it, expect, inject, module */
'use strict';

describe('Job', function () {
  var factory;

  beforeEach(module('job'));

  beforeEach(inject(function (Job) {
    factory = Job;
  }));

  it('should have someValue be Job', function () {
    expect(factory.someValue).toEqual('Job');
  });

  it('should have someMethod return Job', function () {
    expect(factory.someMethod()).toEqual('Job');
  });
});
