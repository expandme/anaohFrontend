(function () {
  'use strict';

  angular
    .module('frontendAnaoh')
    .config(config)
    .constant('servicesConfig', {
      'API_URL': 'http://api.anaoh.com/'
      //'API_URL': 'http://localhost/anaohApi/'
    });

  function config($urlRouterProvider, servicesConfig, $httpProvider, $locationProvider) {

    var API_URL = servicesConfig.API_URL;

    $urlRouterProvider.otherwise('/home');

    $locationProvider.hashPrefix('!')

    $httpProvider.interceptors.push('AuthInterceptor');

  }

}());
