(function () {
  'use strict';

  /**
   * @ngdoc object
   * @name user.controller:LoginCtrl
   *
   * @description
   *
   */
  angular
    .module('user')
    .controller('LoginCtrl', LoginCtrl);

  function LoginCtrl($scope, $state, Auth, SweetAlert, AuthToken, $timeout) {

    $scope.isAuthenticated = AuthToken.isAuthenticated;
    $scope.user = {};
    $scope.user.username = 'xyzdemo';
    $scope.user.password = 'xyz';
    $scope.modalShown = false;
    $scope.toggleModal = function() {
      $scope.modalShown = !$scope.modalShown;
    };

    $scope.submit = function (user) {

      Auth.login(user)
        .success(function (res) {
          $timeout(function() {
            $('.ng-modal-close').trigger('click');
          });
          SweetAlert.swal("Welcome!", "Logged in successfully!", "success");
        })
        .error(function (err) {
          SweetAlert.swal("Error!", err.error.message, "error");
        });
    };

    $scope.logout = function() {
      AuthToken.removeToken();
      $state.go('home');
    }
  }
}());
