(function () {
  'use strict';

  angular
    .module('user.profile')
    .config(config)
    .run(run);

  function config($stateProvider) {
    $stateProvider
      .state('profile-edit', {
        url: '/profile/edit',
        templateUrl: 'user/profile/profile.tpl.html',
        controller: 'ProfileCtrl',
        resolve : {
          loggedin: function(ProfileInfo) {
            return ProfileInfo.checkLoggedOut;
          }
        }
      });
  }

  function run($window, $rootScope, $state) {
    $rootScope.$on('$stateChangeError', function(toState, toParams, fromState, fromParams, error) {
      if (error) {
        $state.go('home');
      };
    })
  }

}());
