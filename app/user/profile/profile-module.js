(function () {
  'use strict';

  /* @ngdoc object
   * @name user.profile
   * @description
   *
   */
  angular
    .module('user.profile', [
      'ui.router'
    ]);
}());
