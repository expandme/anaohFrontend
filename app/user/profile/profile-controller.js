(function () {
  'use strict';

  /**
   * @ngdoc object
   * @name user.profile.controller:ProfileCtrl
   *
   * @description
   *
   */
  angular
    .module('user.profile')
    .controller('ProfileCtrl', ProfileCtrl);

  function ProfileCtrl(Asset, ProfileInfo, $scope, AuthToken) {

    $scope.isAuthenticated = AuthToken.isAuthenticated;
    $scope.user = {};

    return getRoleGeo().then(function() {
      getProfile()
      renderUi();
    });

    function getRoleGeo() {
      return Asset.listRoleGeo().then(function(data) {
        $scope.roles = data.Role;
        $scope.locations = data.Geo;
      });
    }


    function getProfile() {
      return ProfileInfo.me().then(function(data) {
        $scope.user = data.data.userDetails;
        $scope.user.username = data.data.username.username;
        $('.user-location').val(data.data.userDetails.geoId);
      });
    }

    function renderUi() {
      $scope.currentTab = 'account.tpl.html';

      $scope.onClickTab = function (tab) {
        console.log(tab);
        $scope.currentTab = tab.template;
      }

      $scope.isActiveTab = function(tabUrl) {
        return tabUrl == $scope.currentTab;
      }

    }
  }
}());
