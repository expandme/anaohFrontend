/* global describe, beforeEach, it, expect, inject, module */
'use strict';

describe('ProfileInfo', function () {
  var service;

  beforeEach(module('user.profile'));

  beforeEach(inject(function (ProfileInfo) {
    service = ProfileInfo;
  }));

  it('should equal ProfileInfo', function () {
    expect(service.get()).toEqual('ProfileInfo');
  });
});
