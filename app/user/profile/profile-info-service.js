(function () {
  'use strict';

  /**
   * @ngdoc service
   * @name user.profile.service:ProfileInfo
   *
   * @description
   *
   */
  angular
    .module('user.profile')
    .service('ProfileInfo', ProfileInfo);

  function ProfileInfo(servicesConfig, $q, $timeout, $http, $location, $rootScope) {

    var API_URL = servicesConfig.API_URL;

    var service = {
      me: me,
      checkLoggedOut : checkLoggedOut
    };

    return service;

    function me() {
      return $http.get(API_URL + 'users/me')
        .then(getProfileDetails);

      function getProfileDetails(data, status, headers, config) {
        return data;
      }
    }

    function checkLoggedOut() {

      var deferred;
      deferred = $q.defer();

      if ($rootScope.user) {
        deferred.resolve();
      } else {
        $http.get(API_URL + 'users/me').success(function(user) {
          if (user) {
            return $timeout(deferred.resolve);
          } else {
            $timeout(deferred.reject);
            return $location.url('/home');
          }
        }).error(function() {
          $timeout(deferred.reject);
          return $location.url('/home');
        });
      }

      return deferred.promise;
    }

  }
}());
