(function () {
  'use strict';

  /**
   * @ngdoc service
   * @name user.factory:UserRole
   *
   * @description
   *
   */
  angular
    .module('user')
    .factory('UserRole', UserRole);

  function UserRole(servicesConfig, $http, $location, $q) {

    var API_URL = servicesConfig.API_URL;

    var service = {
      associatedUserRoles: associatedUserRoles,
      associatedUserProducts: associatedUserProducts
    };

    return service;

    // function associatedUserRolesById(user_id) {
    //   return $http.get(API_URL + 'role-type/'+user_id)
    //     .then(getList);

    //   function getList(data, status, headers, config) {
    //     return data.data;
    //   }
    // }

    // function addNewUser(user) {
    //   return $http.post(API_URL + 'role-type/add-user', user)
    //     .then(status);

    //   function status(data, status, headers, config) {
    //     return data;
    //   }
    // }

    function associatedUserRoles() {
      return $http.get(API_URL + 'users/associated-user-role')
        .then(getList);

      function getList(data, status, headers, config) {
        return data.data;
      }
    }

    function associatedUserProducts() {
      return $http.get(API_URL + 'users/associated-user-product')
        .then(getList);

      function getList(data, status, headers, config) {
        return data.data;
      }
    }
  }
}());
