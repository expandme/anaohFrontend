/* global describe, beforeEach, it, expect, inject, module */
'use strict';

describe('AuthToken', function () {
  var factory;

  beforeEach(module('user'));

  beforeEach(inject(function (AuthToken) {
    factory = AuthToken;
  }));

  it('should have someValue be AuthToken', function () {
    expect(factory.someValue).toEqual('AuthToken');
  });

  it('should have someMethod return AuthToken', function () {
    expect(factory.someMethod()).toEqual('AuthToken');
  });
});
