(function () {
  'use strict';

  /**
   * @ngdoc service
   * @name user.factory:AuthInterceptor
   *
   * @description
   *
   */
  angular
    .module('user')
    .factory('AuthInterceptor', AuthInterceptor);

  function AuthInterceptor(AuthToken) {
    return {
      request: function (config) {
        var token = AuthToken.getToken();
        if (token) {
          config.headers.Authorization = 'Bearer ' + token;
        }
        return config;

      },
      response: function (response) {
        return response;
      }
    };
  }
}());
