/* global describe, beforeEach, it, expect, inject, module */
'use strict';

describe('AuthInterceptor', function () {
  var factory;

  beforeEach(module('user'));

  beforeEach(inject(function (AuthInterceptor) {
    factory = AuthInterceptor;
  }));

  it('should have someValue be AuthInterceptor', function () {
    expect(factory.someValue).toEqual('AuthInterceptor');
  });

  it('should have someMethod return AuthInterceptor', function () {
    expect(factory.someMethod()).toEqual('AuthInterceptor');
  });
});
