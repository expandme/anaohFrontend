(function () {
  'use strict';

  /* @ngdoc object
   * @name user.dashboard
   * @description
   *
   */
  angular
    .module('user.dashboard', [
      'ui.router',
      'angucomplete-alt'
    ]);
}());
