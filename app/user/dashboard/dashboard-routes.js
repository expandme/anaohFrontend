(function () {
  'use strict';

  angular
    .module('user.dashboard')
    .config(config);

  function config($stateProvider) {
    $stateProvider
      .state('dashboard', {
        url: '/dashboard',
        templateUrl: 'user/dashboard/dashboard.tpl.html',
        controller: 'DashboardCtrl',
        controllerAs: 'dashboard'
      })
      .state('manageScript', {
        url: '/dashboard/:user_id/:script_id',
        templateUrl: 'user/dashboard/manage-script.tpl.html',
        controller: 'ManageScriptCtrl',
      });
  }
}());
