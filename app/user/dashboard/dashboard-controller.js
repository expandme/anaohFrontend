(function () {
  'use strict';

  /**
   * @ngdoc object
   * @name user.dashboard.controller:DashboardCtrl
   *
   * @description
   *
   */
  angular
    .module('user.dashboard')
    .controller('DashboardCtrl', DashboardCtrl);

  function DashboardCtrl($scope, $state, Auth, SweetAlert, AuthToken, $timeout, servicesConfig, Asset, UserRole) {

    $scope.isAuthenticated = AuthToken.isAuthenticated;
    $scope.request_url = servicesConfig.API_URL+'/users?q=';
    $scope.user = {};

    $scope.tabs = [
      {name: 'My Scripts', url: 'scripts.tpl.html'},
      {name: 'My connections', url: 'connected-scripts.tpl.html'},
    ]

    $scope.currentTab = 'scripts.tpl.html';

    $scope.onClickTab = function (tab) {
      $scope.currentTab = tab.url;
    }

    $scope.isActiveTab = function(tabUrl) {
      return tabUrl == $scope.currentTab;
    }

    if ($scope.isAuthenticated()) {

      // Asset.listRoleGeo().then(function(data) {
      //   $scope.roles = data.Role;
      // });

      Auth.authorDashboard().then(function(data) {
        $scope.scripts = data.data.details;
        $scope.scriptConnections = data.data.connectedScripts;
        if (data.data.userRoles.associatedRoles.indexOf("6") != -1) {
          $scope.isCommunicator = true;
          $scope.tabs.push(
            {name: 'Jobs Replied', url: 'jobs-replied.tpl.html'},
            {name: 'Appointments', url: 'appointments.tpl.html'}
          );

          $scope.repliedJobs = data.data.jobsReplied;
          $scope.appointments = data.data.appointments;
        };
      });

      $scope.viewDetails = function(job) {
        $scope.repliedUserDetails = !$scope.repliedUserDetails;
        $scope.detailsIdx = job.details;
      }

      // $scope.addUser = function(user) {
      //   if(!user.selectedUser)
      //     SweetAlert.swal("Error!", 'user entered is not registered', 'error');
      //   console.log(user);
      // }

      // $scope.selectedUser = function($item) {
      //   if($item.originalObject.id) {
      //     $scope.selectedUserId = $item.originalObject.id;
      //     UserRole.associatedUserRolesById($item.originalObject.id).then(function(data) {
      //       console.log(data);
      //       $scope.user = {
      //         roles: data.associatedRoles
      //       };
      //     });
      //   }
      // }

      // $scope.addUser = function(user) {
      //   user.selectedUserId = $scope.selectedUserId;
      //   if (_.size(user) < 3) {
      //     SweetAlert.swal("Error!", 'some fields missing', 'error');
      //   } else {
      //     console.log('all good')
      //     UserRole.addNewUser(user).then(function(data) {
      //       console.log(data);
      //     });
      //   }
      // }
    }
  }
}());
