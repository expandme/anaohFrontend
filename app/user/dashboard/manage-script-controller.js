(function () {
  'use strict';

  /**
   * @ngdoc object
   * @name user.dashboard.controller:DashboardCtrl
   *
   * @description
   *
   */
  angular
    .module('user.dashboard')
    .controller('ManageScriptCtrl', ManageScriptCtrl);

  function ManageScriptCtrl($scope, $stateParams, Auth, SweetAlert, AuthToken, $timeout, servicesConfig, Asset, UserRole) {

    $scope.isAuthenticated = AuthToken.isAuthenticated;
    var user_id = $stateParams.user_id;
    var script_id = $stateParams.script_id;

    $scope.tabs = [];

    $scope.currentTab = 'jobs-replied.tpl.html';

    $scope.onClickTab = function (tab) {
      $scope.currentTab = tab.url;
    }

    $scope.isActiveTab = function(tabUrl) {
      return tabUrl == $scope.currentTab;
    }

    if ($scope.isAuthenticated()) {

      Auth.manageScript(user_id,script_id).then(function(data) {
        console.log(data);
        if(data.data.status) {
          $scope.tabs.push(
            {name: 'Jobs Replied', url: 'jobs-replied.tpl.html'},
            {name: 'Appointments', url: 'appointments.tpl.html'}
          );
          $scope.repliedJobs = data.data.jobsReplied;
          $scope.appointments = data.data.appointments;
        }
      });

    }
  }
}());
