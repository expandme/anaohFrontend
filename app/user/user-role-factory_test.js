/* global describe, beforeEach, it, expect, inject, module */
'use strict';

describe('UserRole', function () {
  var factory;

  beforeEach(module('user'));

  beforeEach(inject(function (UserRole) {
    factory = UserRole;
  }));

  it('should have someValue be UserRole', function () {
    expect(factory.someValue).toEqual('UserRole');
  });

  it('should have someMethod return UserRole', function () {
    expect(factory.someMethod()).toEqual('UserRole');
  });
});
