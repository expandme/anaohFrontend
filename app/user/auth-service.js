(function () {
  'use strict';

  /**
   * @ngdoc service
   * @name user.service:Auth
   *
   * @description
   *
   */
  angular
    .module('user')
    .service('Auth', Auth);

  function Auth($http, servicesConfig, AuthToken, $state, $window, $q) {

    var API_URL = servicesConfig.API_URL;

    function authSuccessfull(res) {
      AuthToken.setToken(res.data.AuthToken);
      $state.go('dashboard');
    }

    this.login = function (user) {
      return $http.post(API_URL + 'users/authenticate/username', user)
        .success(authSuccessfull);
    }

    this.register = function (newUser) {
      return $http.post(API_URL + 'users', newUser)
        .success(authSuccessfull);
    }

    this.isAuthor = function(product_id) {
      return $http.get(API_URL + 'products/is-author/' + product_id)
        .then(status);

      function status(data, status, headers, config) {
        return data;
      }
    }

    this.authorDashboard = function() {
      return $http.get(API_URL + 'users/dashboard')
        .then(status);

      function status(data, status, headers, config) {
        return data;
      }
    }

    this.manageScript = function(user_id, script_id) {
      return $http.get(API_URL + 'users/dashboard/'+user_id+'/'+script_id)
        .then(status);

      function status(data, status, headers, config) {
        return data;
      }
    }

  }
}());
