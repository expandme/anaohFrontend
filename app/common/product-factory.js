(function () {
  'use strict';

  /**
   * @ngdoc service
   * @name common.factory:Product
   *
   * @description
   *
   */
  angular
    .module('common')
    .factory('Product', Product);

  function Product(servicesConfig, $http, $location, $q) {
    var isPrimed = false;
    var primePromise;
    var API_URL = servicesConfig.API_URL;

    var service = {
      getProducts: getProducts,
      createScript: createScript,
      scriptById: scriptById,
      scriptConnection: scriptConnection,
      connectionRequests: connectionRequests,
      recommendation: recommendation,
      approveConnection: approveConnection,
      makeAppointment: makeAppointment,
      makeInvestment: makeInvestment
    };

    return service;

    var limit = null;
    function getProducts(limit,offset) {
      return $http.get(API_URL + 'products?limit='+limit + '&offset='+offset)
        .then(getProductList);

      function getProductList(data, status, headers, config) {
        return data.data;
      }
    }

    function createScript(script) {
      return $http.post(API_URL + 'products', script)
        .then(status);

      function status(data, status, headers, config) {
        return data;
      }
    }

    function scriptById(id) {
      return $http.get(API_URL + 'products/'+id)
        .then(scriptDetails);

      function scriptDetails(data, status, headers, config) {
        return data;
      }
    }

    function scriptConnection(scriptId) {
      return $http.get(API_URL + 'products/connection/'+scriptId)
        .then(status);

      function status(data, status, headers, config) {
        return data;
      }
    }

    function connectionRequests(scriptId){
      return $http.get(API_URL + 'products/connection-request/'+scriptId)
        .then(status);

      function status(data, status, headers, config) {
        return data;
      }
    }

    function recommendation(data) {
      return $http.post(API_URL + 'products/recommendation', data)
        .then(status);

      function status(data, status, headers, config) {
        return data;
      }
    }

    function approveConnection(data) {
      return $http.post(API_URL + 'products/approve-connection', data)
        .then(status);

      function status(data, status, headers, config) {
        return data;
      }
    }

    function makeAppointment(data) {
      return $http.post(API_URL + 'products/make-appointment', data)
        .then(status);

      function status(data, status, headers, config) {
        return data;
      }
    }

    function makeInvestment(data) {
      return $http.post(API_URL + 'products/make-investment', data)
        .then(status);

      function status(data, status, headers, config) {
        return data;
      }
    }
  }
}());
