/* global describe, beforeEach, it, expect, inject, module */
'use strict';

describe('checklistModel', function () {
  var scope
    , element;

  beforeEach(module('common', 'common/checklist-model-directive.tpl.html'));

  beforeEach(inject(function ($compile, $rootScope) {
    scope = $rootScope.$new();
    element = $compile(angular.element('<checklist-model></checklist-model>'))(scope);
  }));

  it('should have correct text', function () {
    scope.$apply();
    expect(element.isolateScope().checklistModel.name).toEqual('checklistModel');
  });
});
