(function () {
  'use strict';

  /**
   * @ngdoc service
   * @name common.factory:Asset
   *
   * @description
   *
   */
  angular
    .module('common')
    .factory('Asset', Asset);

  function Asset(servicesConfig, $http) {

    var API_URL = servicesConfig.API_URL;

    var service = {
      listRoleGeo: listRoleGeo,
      listGenreGeo: listGenreGeo,
      listJobTypes: listJobTypes
    };

    return service;

    function listRoleGeo() {
      return $http.get(API_URL + 'assets?resource[]=Geo&resource[]=Role')
        .then(getListRoleGeo);

      function getListRoleGeo(data, status, headers, config) {
        return data.data;
      }
    }

    function listGenreGeo() {
      return $http.get(API_URL + 'assets?resource[]=Geo&resource[]=Genre&resource[]=Language')
        .then(getListGenreGeo);

      function getListGenreGeo(data, status, headers, config) {
        return data.data;
      }
    }

    function listJobTypes() {
      return $http.get(API_URL + 'assets?resource[]=JobType')
        .then(getListJobTypes);

      function getListJobTypes(data, status, headers, config) {
        return data.data;
      }
    }
  }
}());
