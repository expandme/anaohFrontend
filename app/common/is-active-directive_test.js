/* global describe, beforeEach, it, expect, inject, module */
'use strict';

describe('isActive', function () {
  var scope
    , element;

  beforeEach(module('common', 'common/is-active-directive.tpl.html'));

  beforeEach(inject(function ($compile, $rootScope) {
    scope = $rootScope.$new();
    element = $compile(angular.element('<is-active></is-active>'))(scope);
  }));

  it('should have correct text', function () {
    scope.$apply();
    expect(element.isolateScope().isActive.name).toEqual('isActive');
  });
});
