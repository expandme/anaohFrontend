/* global describe, beforeEach, it, expect, inject, module */
'use strict';

describe('validateEquals', function () {
  var scope
    , element;

  beforeEach(module('common', 'common/validate-equals-directive.tpl.html'));

  beforeEach(inject(function ($compile, $rootScope) {
    scope = $rootScope.$new();
    element = $compile(angular.element('<validate-equals></validate-equals>'))(scope);
  }));

  it('should have correct text', function () {
    scope.$apply();
    expect(element.isolateScope().validateEquals.name).toEqual('validateEquals');
  });
});
