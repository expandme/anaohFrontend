/* global describe, beforeEach, it, expect, inject, module */
'use strict';

describe('Asset', function () {
  var factory;

  beforeEach(module('common'));

  beforeEach(inject(function (Asset) {
    factory = Asset;
  }));

  it('should have someValue be Asset', function () {
    expect(factory.someValue).toEqual('Asset');
  });

  it('should have someMethod return Asset', function () {
    expect(factory.someMethod()).toEqual('Asset');
  });
});
