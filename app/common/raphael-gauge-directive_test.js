/* global describe, beforeEach, it, expect, inject, module */
'use strict';

describe('raphaelGauge', function () {
  var scope
    , element;

  beforeEach(module('common', 'common/raphael-gauge-directive.tpl.html'));

  beforeEach(inject(function ($compile, $rootScope) {
    scope = $rootScope.$new();
    element = $compile(angular.element('<raphael-gauge></raphael-gauge>'))(scope);
  }));

  it('should have correct text', function () {
    scope.$apply();
    expect(element.isolateScope().raphaelGauge.name).toEqual('raphaelGauge');
  });
});
