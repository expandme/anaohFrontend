/* global describe, beforeEach, it, expect, inject, module */
'use strict';

describe('Product', function () {
  var factory;

  beforeEach(module('common'));

  beforeEach(inject(function (Product) {
    factory = Product;
  }));

  it('should have someValue be Product', function () {
    expect(factory.someValue).toEqual('Product');
  });

  it('should have someMethod return Product', function () {
    expect(factory.someMethod()).toEqual('Product');
  });
});
