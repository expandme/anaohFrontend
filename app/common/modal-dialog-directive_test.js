/* global describe, beforeEach, it, expect, inject, module */
'use strict';

describe('modalDialog', function () {
  var scope
    , element;

  beforeEach(module('common', 'common/modal-dialog-directive.tpl.html'));

  beforeEach(inject(function ($compile, $rootScope) {
    scope = $rootScope.$new();
    element = $compile(angular.element('<modal-dialog></modal-dialog>'))(scope);
  }));

  it('should have correct text', function () {
    scope.$apply();
    expect(element.isolateScope().modalDialog.name).toEqual('modalDialog');
  });
});
