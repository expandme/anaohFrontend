/* global describe, beforeEach, it, expect, inject, module */
'use strict';

describe('ngFlatDatepicker', function () {
  var scope
    , element;

  beforeEach(module('common', 'common/ng-flat-datepicker-directive.tpl.html'));

  beforeEach(inject(function ($compile, $rootScope) {
    scope = $rootScope.$new();
    element = $compile(angular.element('<ng-flat-datepicker></ng-flat-datepicker>'))(scope);
  }));

  it('should have correct text', function () {
    scope.$apply();
    expect(element.isolateScope().ngFlatDatepicker.name).toEqual('ngFlatDatepicker');
  });
});
