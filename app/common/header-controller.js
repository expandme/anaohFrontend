(function () {
  'use strict';

  /**
   * @ngdoc object
   * @name common.controller:HeaderCtrl
   *
   * @description
   *
   */
  angular
    .module('common')
    .controller('HeaderCtrl', HeaderCtrl);

  function HeaderCtrl($scope, $http, servicesConfig) {

    var API_URL = servicesConfig.API_URL;

    $scope.settings = {
      closeEl: '.close',
      overlay: {
        templateUrl: 'common/morphSearch.tpl.html',
        scroll: false
      }
    }

    $scope.search = {
      searchQ : "",
      results : {},
      search : function(){
        if($scope.search.searchQ.length < 3) return;
        $http.get(API_URL+"search?q="+$scope.search.searchQ).success(function(data, status){
          // console.log(data);
          $scope.search.results = data;
          console.log($scope.search.results.scripts.length);
        }).error(function(data, status){
          console.log(data+" "+status);
        });
      }
    };
  }
}());
