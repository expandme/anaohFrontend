(function () {
  'use strict';

  /**
   * @ngdoc directive
   * @name common.directive:validateEquals
   * @restrict EA
   * @element
   *
   * @description
   *
   * @example
     <example module="common">
       <file name="index.html">
        <validate-equals></validate-equals>
       </file>
     </example>
   *
   */
  angular
    .module('common')
    .directive('validateEquals', validateEquals);

  function validateEquals() {
    return {
      require: 'ngModel',
      link: function (scope, element, attrs, ngModelCtrl) {
        function validateEqual(value) {
          var valid = (value === scope.$eval(attrs.validateEquals));
          ngModelCtrl.$setValidity('equal', valid);
          return valid ? value : undefined;
        }
        ngModelCtrl.$parsers.push(validateEqual);
        ngModelCtrl.$formatters.push(validateEqual);

        scope.$watch(attrs.validateEquals, function () {
          ngModelCtrl.$setViewValue(ngModelCtrl.$viewValue);
        })
      }
    };
  }
}());
