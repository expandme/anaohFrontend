(function () {
  'use strict';

  /**
   * @ngdoc directive
   * @name common.directive:modalDialog
   * @restrict EA
   * @element
   *
   * @description
   *
   * @example
     <example module="common">
       <file name="index.html">
        <modal-dialog></modal-dialog>
       </file>
     </example>
   *
   */
  angular
    .module('common')
    .directive('modalDialog', modalDialog);

  function modalDialog() {
    return {
      restrict: 'E',
      scope: {
        show: '='
      },
      replace: true, // Replace with the template below
      transclude: true, // we want to insert custom content inside the directive
      link: function(scope, element, attrs) {
        scope.dialogStyle = {};
        if (attrs.width)
          scope.dialogStyle.width = attrs.width;
        if (attrs.height)
          scope.dialogStyle.height = attrs.height;
        if (attrs.overflow)
          scope.dialogStyle.overflow = attrs.overflow;
        scope.hideModal = function() {
          scope.show = false;
        };
      },
      template: '<div class="ng-modal" ng-show="show"><div ng-click="hideModal()""></div><div class="ng-modal-dialog" ng-style="dialogStyle"><a class="ng-modal-close" ng-click="hideModal()"><i class="fa fa-times"></i></a><div class="ng-modal-dialog-content" ng-transclude></div></div></div>'
    };
  }
}());
