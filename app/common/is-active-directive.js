(function () {
  'use strict';

  /**
   * @ngdoc directive
   * @name common.directive:isActive
   * @restrict EA
   * @element
   *
   * @description
   *
   * @example
     <example module="common">
       <file name="index.html">
        <is-active></is-active>
       </file>
     </example>
   *
   */
  angular
    .module('common')
    .directive('isActive', isActive);

  function isActive() {
    return {
      restrict: 'A',
      link: function (scope, element, attrs) {
        element.click(function() {
          $(element).addClass("item-active");
          $(element).siblings().removeClass("item-active");
        });
      }
    };
  }
}());
