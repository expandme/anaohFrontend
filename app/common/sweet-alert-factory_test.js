/* global describe, beforeEach, it, expect, inject, module */
'use strict';

describe('SweetAlert', function () {
  var factory;

  beforeEach(module('common'));

  beforeEach(inject(function (SweetAlert) {
    factory = SweetAlert;
  }));

  it('should have someValue be SweetAlert', function () {
    expect(factory.someValue).toEqual('SweetAlert');
  });

  it('should have someMethod return SweetAlert', function () {
    expect(factory.someMethod()).toEqual('SweetAlert');
  });
});
