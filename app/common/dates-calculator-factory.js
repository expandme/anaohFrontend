(function () {
  'use strict';

  /**
   * @ngdoc service
   * @name common.factory:DatesCalculator
   *
   * @description
   *
   */
  angular
    .module('common')
    .factory('DatesCalculator', DatesCalculator);

  function DatesCalculator() {
    /**
     * List all years for the select
     * @return {[type]} [description]
     */
    function getYearsList() {
      var yearsList = [];
      for (var i = 2005; i <= moment().year(); i++) {
        yearsList.push(i);
      }
      return yearsList;
    }

    /**
     * List all days name in the current locale
     * @return {[type]} [description]
     */
    function getDaysNames () {
      var daysNameList = [];
      for (var i = 0; i < 7 ; i++) {
        daysNameList.push(moment().weekday(i).format('ddd'));
      }
      return daysNameList;
    }

    return {
      getYearsList: getYearsList,
      getDaysNames: getDaysNames
    };

  }
}());
