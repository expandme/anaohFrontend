(function () {
  'use strict';

  /**
   * @ngdoc object
   * @name common.controller:CommonCtrl
   *
   * @description
   *
   */
  angular
    .module('common')
    .controller('CommonCtrl', CommonCtrl);

  function CommonCtrl() {
    var vm = this;
    vm.ctrlName = 'CommonCtrl';
  }
}());
