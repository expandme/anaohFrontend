/* global describe, beforeEach, it, expect, inject, module */
'use strict';

describe('angularStarRating', function () {
  var scope
    , element;

  beforeEach(module('common', 'common/angular-star-rating-directive.tpl.html'));

  beforeEach(inject(function ($compile, $rootScope) {
    scope = $rootScope.$new();
    element = $compile(angular.element('<angular-star-rating></angular-star-rating>'))(scope);
  }));

  it('should have correct text', function () {
    scope.$apply();
    expect(element.isolateScope().angularStarRating.name).toEqual('angularStarRating');
  });
});
