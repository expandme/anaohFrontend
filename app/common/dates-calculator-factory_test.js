/* global describe, beforeEach, it, expect, inject, module */
'use strict';

describe('DatesCalculator', function () {
  var factory;

  beforeEach(module('common'));

  beforeEach(inject(function (DatesCalculator) {
    factory = DatesCalculator;
  }));

  it('should have someValue be DatesCalculator', function () {
    expect(factory.someValue).toEqual('DatesCalculator');
  });

  it('should have someMethod return DatesCalculator', function () {
    expect(factory.someMethod()).toEqual('DatesCalculator');
  });
});
